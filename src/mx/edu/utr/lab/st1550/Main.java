package mx.edu.utr.lab.st1550;

import static mx.edu.utr.lab.st1550.Sorting.bubbleSort;

public class Main {

    public static void main(String[] args) {

        int[] array = {5, 4, 3, 2, 1, 7, 11};
        int[] array100 = new int[100];
        int[] array1000 = new int[1000];
        int[] array10000 = new int[10000];
        int[] array100000 = new int[100000];

        /*int c = 0;
        while (c <= array.length - 1) {
            System.out.println(array[c]);
            c++;
        }
         */
        int i = 100;
        int pos = 0;
        while (i > 0) {
            array100[pos] = i;
            i--;
            //System.out.println(array100[pos]);
            pos++;

        }
        i = 1000;
        pos = 0;
        while (i > 0) {
            array1000[pos] = i;
            i--;
            //System.out.println(array1000[pos]);
            pos++;

        }

        i = 10000;
        pos = 0;
        while (i > 0) {
            array10000[pos] = i;
            i--;
            //System.out.println(array10000[pos]);
            pos++;

        }

        i = 100000;
        pos = 0;
        while (i > 0) {
            array100000[pos] = i;
            i--;
            //System.out.println(array100000[pos]);
            pos++;

        }

        
        System.out.println("SORTING 7 elements Array:");
        long startTime = System.nanoTime();
        Sorting.bubbleSort(array);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("This process took: " + duration + " nanoseconds\n");
        
        System.out.println("SORTING 100 elements Array:");
        startTime = System.nanoTime();
        Sorting.bubbleSort(array100);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.println("This process took: " + duration + " nanoseconds\n");

        System.out.println("SORTING 1,000 elements Array:");
        startTime = System.nanoTime();
        Sorting.bubbleSort(array1000);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.println("This process took: " + duration + " nanoseconds\n");

       System.out.println("SORTING 10,000 elements Array:");
        startTime = System.nanoTime();
        Sorting.bubbleSort(array10000);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.println("This process took: " + duration + " nanoseconds\n");

        System.out.println("SORTING 100,000 elements Array:");
        startTime = System.nanoTime();
        Sorting.bubbleSort(array100000);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.println("This process took: " + duration + " nanoseconds\n");

        /*int count = 0;
        while (count < 100) {
            System.out.println(array100[count]);
            count++;
        }
         */

 /*System.out.println("Array100");
        int c2 = 0;
        while (c2 <= array100.length - 1) {
            System.out.println(array100[c2]);
            c2++;
        }
         */

 /*
         int arr[] ={3,60,35,2,45,320,5};  
                 
         System.out.println("Array Before Bubble Sort");  
         for(int i=0; i < arr.length; i++){  
         System.out.print(arr[i] + " ");  
         }  
         System.out.println();  
                  
         bubbleSort(arr);//sorting array elements using bubble sort  
                 
         System.out.println("Array After Bubble Sort");  
         for(int i=0; i < arr.length; i++){  
         System.out.print(arr[i] + " ");           
                        
         }  
         */
    }
}
